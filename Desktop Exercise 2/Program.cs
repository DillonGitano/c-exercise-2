﻿using System;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      ArrayTester();

      Console.Read();
    }

    private static decimal[] ArrayGenerator()
    {

      return ArrayFactory.GetArray();
    }

    private static void ArrayTester()
    {
      var arr = ArrayGenerator();

      Console.WriteLine("**** OutputArray ****");

      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n**** AverageArrayValue ****");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.AverageArrayValue(arr)));

      Console.WriteLine("\r\n**** MinArrayValue ****");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.MinArrayValue(arr)));

      Console.WriteLine("\r\n**** MaxArrayValue ****");

      Console.WriteLine(string.Format("{0:C}", ArrayFactory.MaxArrayValue(arr)));

      Console.WriteLine("\r\n**** SortArrayAsc ****");

      ArrayFactory.SortArrayAsc(arr);

      ArrayFactory.OutputArray(arr);
    }
  }
}