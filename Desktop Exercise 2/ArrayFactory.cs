﻿using System;
using System.Linq;


namespace Desktop_Exercise_2
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>

    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of 10 random numbers
    /// </summary>
    /// <returns></returns>

    public static decimal[] GetArray()
    {
      var arr = new decimal[10];

      for (int i = 0; i < arr.Length; i++)
      {
        arr[i] = RandomNumberBetween((decimal)0.49, (decimal)149.99);
      }

      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>

    public static void OutputArray(decimal[] arr)
    {
      for (var i = 0; i < arr.Length; i++)
      {
        Console.WriteLine(string.Format("{0:C}", arr[i]));
      }
    }

    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>

    public static decimal AverageArrayValue(decimal[] arr)
    {
      var sum = (decimal)0;
      var i = 0;

      do
      {
        sum += arr[i];
        i++;
      } while (i < arr.Length);

      return sum / arr.Length;
    }

    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>

    public static decimal MaxArrayValue(decimal[] arr)
    {
      var maxVal = (decimal)0;

      foreach (var a in arr)
      {
        if (a > maxVal)
        {
          maxVal = a;
        }
      }

      return maxVal;
    }


    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>

    public static decimal MinArrayValue(decimal[] arr)
    {
      var minVal = arr[0];
      var i = 0;

      while (i < arr.Length)
      {
        if (arr[i] < minVal)
        {
          minVal = arr[i];
        }

        i += 1;
      }

      return minVal;
    }

    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>

    public static void SortArrayAsc(decimal[] arr)
    {

      //  do 
      //  {
      //  string reply;
      //  Console.WriteLine("Please enter 'ascend' or 'descend'");
      //  reply = Console.ReadLine();

      //  //loop through the entire array
      //    for (var i = 0; i < arr.Length; i++)
      //    {
      //      //loop through the array's "next" elements
      //      for (var j = 0; j < arr.Length - 1; j++)
      //      {
      //        //if the item at j is greater than the next item, switch the items around
      //        if (arr[j] > arr[j + 1])
      //        {
      //          var x = arr[j + 1];

      //          arr[j + 1] = arr[j];
      //          arr[j] = x;
      //        }
      //      }
      //    }

      //    while
      //      ( reply == "ascend" || reply == "descend");

      //    Console.WriteLine("Would you like to re-sort ?");

      //  }
      //}

      string reply;
      Console.WriteLine("Please enter 'ascend' or 'descend'");
      reply = Console.ReadLine();

      if (reply == "ascend")
      {
        //loop through the entire array
        for (var i = 0; i < arr.Length; i++)
        {
          //loop through the array's "next" elements
          for (var j = 0; j < arr.Length - 1; j++)
          {
            //if the item at j is greater than the next item, switch the items around
            if (arr[j] > arr[j + 1])
            {
              var x = arr[j + 1];

              arr[j + 1] = arr[j];
              arr[j] = x;
            }
          }
        }
      }

      if (reply == "descend")
      {
        //loop through the entire array
        for (var i = 0; i < arr.Length; i++)
        {
          //loop through the array's "next" elements
          for (var j = 0; j < arr.Length - 1; j++)
          {
            //if the item at j is less than the next item, switch the items around
            if (arr[j] < arr[j + 1])
            {
              var x = arr[j + 1];

              arr[j + 1] = arr[j];
              arr[j] = x;
            }
          }
        }
      }
    }


    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}
